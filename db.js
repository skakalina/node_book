//экспорт магуста
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/database_1'; //ссылка на базуданных
mongoose.connect(mongoDB); //подключение базы данных
const Schema = mongoose.Schema; //получение схемы в переменную
mongoose.Promise = global.Promise;
//обработка ошибка
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
//экспорт
module.exports.Schema = Schema;
module.exports.mongoose = mongoose;