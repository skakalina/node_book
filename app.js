//Импорт библиотек
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
//Добавление файлов для работы с маршрутами
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var catalogRouter = require('./routes/catalog');
//Создание объекта приложения с помощью модуля exspress
var app = express();

// Механизмнастроки приложений
app.set('views', path.join(__dirname, 'views')); //устанавливаем значение 'views', чтобы указать папку, в которой будут храниться шаблоны
app.set('view engine', 'pug'); //формат шаблонов
// для обработки middleware
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'))); // для получения статики
//определение маршрутов для различных статей
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/catalog', catalogRouter);
// отлавливает и обрабатывает ошибки
app.use(function(req, res, next) {
  next(createError(404));
});

// обработчик ошибок
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // отображение страницы с ошибкой
  res.status(err.status || 500);
  res.render('error');
});
//экспортирует для работы с bin
module.exports = app;
