var Genre = require('../models/genre');
var Book = require('../models/book');
var async = require('async');
const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');


exports.genre_list = function(req, res, next) {
    Genre.find()
        .sort([['name', 'ascending']])
        .exec(function (err, genre_list) { //обрабатывает ошибки
            if (err) { return next(err); } //если ошибка обрабатывает ее
            //Successful, so render
            res.render('author_list', { title: 'Genre List', author_list: genre_list }); //в ином случае выводит html страницу
        });
};

exports.genre_detail = function(req, res, next) {

    async.parallel({
        genre: function(callback) {
            Genre.findById(req.params.id)
                .exec(callback);
        },

        genre_books: function(callback) {
            Book.find({ 'genre': req.params.id })
                .exec(callback);
        },

    }, function(err, results) {
        if (err) { return next(err); }
        if (results.genre==null) { // No results.
            var err = new Error('Genre not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render
        res.render('genre_detail', { title: 'Genre Detail', genre: results.genre, genre_books: results.genre_books } );
    });

};

exports.genre_create_get = function(req, res, next) {
    res.render('genre_form', { title: 'Create Genre' });
};

// обработчик жанра созданного POST.
exports.genre_create_post =  [

    // Убедитесь, что поле имя не пустое.
    body('name', 'Genre name required').isLength({ min: 1 }).trim(),

    // Обработка запроса после проверки и очистки.
    sanitizeBody('name').trim().escape(),

    // Извлеките ошибки проверки из запроса.
    (req, res, next) => {

        // Извлеките ошибки проверки из запроса.
        const errors = validationResult(req);

        // Создайте объект жанра с экранированными и обрезанными данными.
        var genre = new Genre(
            { name: req.body.name }
        );


        if (!errors.isEmpty()) {
            // Создайте объект жанра с экранированными и обрезанными данными.
            res.render('genre_form', { title: 'Create Genre', genre: genre, errors: errors.array()});
            return;
        }
        else {
            // Данные из формы действительны.
            // Проверьте, существует ли Жанр с таким же названием.
            Genre.findOne({ 'name': req.body.name })
                .exec( function(err, found_genre) {
                    if (err) { return next(err); }

                    if (found_genre) {
                        // Жанр существует, перенаправление на его страницу.
                        res.redirect(found_genre.url);
                    }
                    else {

                        genre.save(function (err) {
                            if (err) { return next(err); }
                            // Жанр сохранен. Перенаправление на страницу сведений о жанре.
                            res.redirect(genre.url);
                        });

                    }

                });
        }
    }
];

exports.genre_delete_get = function(req, res) {
    async.parallel({
        genre: function(callback) {
            Genre.findById(req.params.id).exec(callback) //ищет жанр по id и проверяет что он существует
        },
        genres_books: function(callback) {
            Book.find({ 'genre': req.params.id }).exec(callback) //ищет книги с данным автором и возвращает либо null либо результат
        },
    }, function(err, results) { //проверяет условия на ошибки
        if (err) { return next(err); } //если есть ошибка ввыводит результат
        if (results.genre==null) { // если нет автора.
            res.redirect('/catalog/genres'); //то перенаправляет на каталог авторов
        }
        // Удачно, значит рендерим.
        res.render('genre_delete', { title: 'Delete Genre', genre: results.genre, genres_books: results.genres_books } );
    });

};

exports.genre_delete_post = function(req, res, next) {

    async.parallel({
        genre: function(callback) {
            Genre.findById(req.body.genreid).exec(callback)
        },
        genres_books: function(callback) {
            Book.find({ 'genre': req.body.genreid }).exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); } //выводим ошибку
        // Success
        if (results.genres_books.length > 0) {
            // Автор книги. Визуализация выполняется так же, как и для GET route.
            res.render('genre_delete', { title: 'Delete Genre', genre: results.genre, genres_books: results.genres_books } );
            return;
        }
        else {
            //У автора нет никаких книг. Удалить объект и перенаправить в список авторов.
            Genre.findByIdAndRemove(req.body.genreid, function deleteGenre(err) {
                if (err) { return next(err); }
                // Успех-перейти к списку авторов
                res.redirect('/catalog/genres')
            })
        }
    });
};

exports.genre_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update GET');
};

exports.genre_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update POST');
};