var Author = require('../models/author'); //импортируем модель автор
var async = require('async');
var Book = require('../models/book');
const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');


// Показать список всех авторов.
exports.author_list = function(req, res, next) {

    Author.find() //ищет все модели
        .sort([['family_name', 'ascending']]) //сорртирует модели по family_name
        .exec(function (err, list_authors) { //обрабатывает ошибки
            if (err) { return next(err); } //если ошибка обрабатывает ее
            //Successful, so render
            res.render('author_list', { title: 'Author List', author_list: list_authors }); //в ином случае выводит html страницу
        });

};
// Отображение страницы сведений для конкретного автора
exports.author_detail = function(req, res, next) {

    async.parallel({
        author: function(callback) {
            Author.findById(req.params.id)
                .exec(callback)
        },
        authors_books: function(callback) {
            Book.find({ 'author': req.params.id },'title summary')
                .exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.author==null) { // No results.
            var err = new Error('Author not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render.
        res.render('author_detail', { title: 'Author Detail', author: results.author, author_books: results.authors_books } );
    });

};

// Отображение создание форм автора GET.
exports.author_create_get = function(req, res, next) {
    res.render('author_form', { title: 'Create Author'});
};

// обработчик созданного авотра POST.
exports.author_create_post = [

    // Валидация полей
    body('first_name').isLength({ min: 1 }).trim().withMessage('First name must be specified.')
        .isAlphanumeric().withMessage('First name has non-alphanumeric characters.'),
    body('family_name').isLength({ min: 1 }).trim().withMessage('Family name must be specified.')
        .isAlphanumeric().withMessage('Family name has non-alphanumeric characters.'),
    body('date_of_birth', 'Invalid date of birth').optional({ checkFalsy: true }).isISO8601(),
    body('date_of_death', 'Invalid date of death').optional({ checkFalsy: true }).isISO8601(),

    // Очистка полей
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('family_name').trim().escape(),
    sanitizeBody('date_of_birth').toDate(),
    sanitizeBody('date_of_death').toDate(),

    // Обработка запроса после проверки и очистки.
    (req, res, next) => {

        // Извлеките ошибки проверки из запроса.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // Есть ошибки. Снова отрисовка формы с обработанными значениями / сообщениями об ошибках..
                res.render('author_form', { title: 'Create Author', author: req.body, errors: errors.array()});
            return;
        }
        else {
            // Данные из формы действительны

            // Создайте объект Author с экранированными и усеченными данными.
            var author = new Author(
                {
                    first_name: req.body.first_name,
                    family_name: req.body.family_name,
                    date_of_birth: req.body.date_of_birth,
                    date_of_death: req.body.date_of_death
                });
            author.save(function (err) {
                if (err) { return next(err); }
                // Успешно-перенаправление на новую запись автора.
                res.redirect(author.url);
            });
        }
    }
];

// Отобразить форму удаления автора метод GET.
exports.author_delete_get = function(req, res, next) {

    async.parallel({
        author: function(callback) {
            Author.findById(req.params.id).exec(callback) //ищет автора по id и проверяет что он существует
        },
        authors_books: function(callback) {
            Book.find({ 'author': req.params.id }).exec(callback) //ищет книги с данным автором и возвращает либо null либо результат
        },
    }, function(err, results) { //проверяет условия на ошибки
        if (err) { return next(err); } //если есть ошибка ввыводит результат
        if (results.author==null) { // если нет автора.
            res.redirect('/catalog/authors'); //то перенаправляет на каталог авторов
        }
        // Удачно, значит рендерим.
        res.render('author_delete', { title: 'Delete Author', author: results.author, author_books: results.authors_books } );
    });

};

// Обработчик удаления автора POST.
exports.author_delete_post = function(req, res, next) {

    async.parallel({
        author: function(callback) {
            Author.findById(req.body.authorid).exec(callback)
        },
        authors_books: function(callback) {
            Book.find({ 'author': req.body.authorid }).exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); } //выводим ошибку
        // Success
        if (results.authors_books.length > 0) {
            // Автор книги. Визуализация выполняется так же, как и для GET route.
            res.render('author_delete', { title: 'Delete Author', author: results.author, author_books: results.authors_books } );
            return;
        }
        else {
            //У автора нет никаких книг. Удалить объект и перенаправить в список авторов.
            Author.findByIdAndRemove(req.body.authorid, function deleteAuthor(err) {
                if (err) { return next(err); }
                // Успех-перейти к списку авторов
                res.redirect('/catalog/authors')
            })
        }
    });
};

// обновление форм автора GET.
exports.author_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author update GET');
};

// обработчик обновления автора POST.
exports.author_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author update POST');
};