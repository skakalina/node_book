const express = require('express');
const {Author} = require('../models/author');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.redirect('/catalog');
});

module.exports = router;
