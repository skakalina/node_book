var express = require('express'); //загрузка модуля для получения express.Router
var router = express.Router(); //импортируем маршрут

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/cool', function(req, res, next) {
    res.send('You\'re so cool');
});

module.exports = router;
